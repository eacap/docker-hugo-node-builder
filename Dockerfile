FROM alpine:latest

MAINTAINER mva3212

RUN apk add --no-cache \
    curl \
    git \
    openssh-client \
    rsync \
    npm \
    nodejs \
    libc6-compat \
    g++
    
RUN apk -Uuv add groff less python py-pip
RUN pip install awscli
RUN apk --purge -v del py-pip
RUN rm /var/cache/apk/*
CMD ["/bin/bash"]

VOLUME ${HOME}/.aws

ENTRYPOINT ["aws"]

ENV VERSION 0.69.0
RUN mkdir -p /usr/local/src \
    && cd /usr/local/src \

    && curl -L https://github.com/gohugoio/hugo/releases/download/v${VERSION}/hugo_extended_${VERSION}_linux-64bit.tar.gz | tar -xz \
    && mv hugo /usr/local/bin/hugo \

    && curl -L https://bin.equinox.io/c/dhgbqpS8Bvy/minify-stable-linux-amd64.tgz | tar -xz \
    && mv minify /usr/local/bin/ \

    && addgroup -Sg 1000 hugo \
    && adduser -SG hugo -u 1000 -h /src hugo

WORKDIR /src

EXPOSE 1313